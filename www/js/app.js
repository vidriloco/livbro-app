

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.factories', 'ngCordova', 'ngOpenFB', 'ngResource'])

.run(function($ionicPlatform, ngFB, $rootScope, LivbroApp, $localstorage) {
	$rootScope.systemIsBusy = false;
	
	$rootScope.apiURL = function() {
		return LivbroApp.host.production;
	}
	
	$rootScope.systemBusy = function() {
		return $rootScope.systemIsBusy;
	}
	
	$rootScope.setSystemToBusy = function(busy) {
		$rootScope.systemIsBusy = busy;
	}
	
	$rootScope.userHasSession = function() {
		return $localstorage.getObject('user') != null && (typeof $localstorage.getObject('user').token != "undefined");
	}
	
	$rootScope.currentUserIsSameAs = function(user) {
		return $rootScope.userHasSession() && $localstorage.getObject('user').id == user.id;
	}
	
	$rootScope.humanUserIdentity = function(user) {
		if(typeof user != "undefined" && user != null)
			return user.first_name+" "+user.last_name;
		else
			return "";
	}
		
  $ionicPlatform.ready(function() {
    ngFB.init({appId: '710735329061215'});
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($compileProvider){
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|content|cdvfile):|data:image\//);
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
  // Each tab has its own nav history stack:
  .state('tab', {
  	url: '/tab',
  	abstract: true,
  	templateUrl: 'templates/tabs.html'
	})
  .state('tab.home', {
    url: '/home',
    views: {
      'tab-home': {
        templateUrl: 'templates/timeline.html',
        controller: 'TimelineController'
      }
    }
  })
  .state('tab.book-detail', {
      url: '/books/:bookID',
      views: {
        'tab-home': {
          templateUrl: 'templates/book-details.html',
          controller: 'BookDetailsController'
        }
      }
  })
  .state('tab.profile', {
    url: '/profile',
    views: {
      'tab-profile': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileController'
      }
    }
  })
  .state('tab.reserves', {
      url: '/reserves',
      views: {
        'tab-reserves': {
          templateUrl: 'templates/reserves.html',
          controller: 'ReservesController'
        }
      }
  })
  .state('tab.reserve-detail', {
      url: '/reserves/:reserveID',
      views: {
        'tab-reserves': {
          templateUrl: 'templates/reserve-details.html',
          controller: 'ReserveDetailsController'
        }
      }
  })
  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginController'
  })
  .state('signup', {
    url: '/sign-up',
    templateUrl: 'templates/sign-up.html',
    controller: 'SignUpController'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});

app.directive('limitChar', function() {
    'use strict';
    return {
        restrict: 'A',
        scope: {
            limit: '=limit',
            ngModel: '=ngModel'
        },
        link: function(scope) {
            scope.$watch('ngModel', function(newValue, oldValue) {
                if (newValue) {
                    var length = newValue.toString().length;
                    if (length > scope.limit) {
                        scope.ngModel = oldValue;
                    }
                }
            });
        }
    };
})

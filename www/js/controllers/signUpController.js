controllers.controller('SignUpController', function($scope, ngFB, $state, User, $localstorage, $http) {
  if($scope.userHasSession()) {
    $state.go('tab.home', { });
  }
  
  $scope.data = {};
	
	$scope.signup = function() {
		$scope.setSystemToBusy(true);
		var registrationParams = {
			registration: { email: $scope.data.email, password: $scope.data.password, first_name: $scope.data.firstName, last_name: $scope.data.lastName }
		}
		$http.post($scope.apiURL().concat('/sign-up.json'), registrationParams).then(function(res) {
			$localstorage.setObject('user', { token: res.data.token, identifier: res.data.identifier }, function() {
				$state.go('tab.home', { });
			});
			$scope.setSystemToBusy(false);
		}, function(err) {
			$scope.setSystemToBusy(false);
		});
	}
});
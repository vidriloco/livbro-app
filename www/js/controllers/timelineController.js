controllers.controller('TimelineController', function($scope, $ionicHistory, $state, BookService, $ionicPlatform, $ionicModal, $cordovaBarcodeScanner, $http, $ionicActionSheet, ImageFactory, $localstorage, $cordovaGeolocation) {
	
  $scope.noMoreItemsAvailable = false;
	$scope.currentBookData = null;
	$scope.newBookData = BookService.new();
	
	$scope.location = null;
	
	$scope.search = {
		term: null
	}
	
	$scope.books = function() {
		return BookService.all;
	}
	
	$scope.loadMore = function() {
	  $scope.fetchBooks(5);
	}
	
  $scope.$on('$stateChangeSuccess', function() {
    $scope.loadMore();
  });
	
	// For in the future displaying a list of possible matched books
	$scope.page = 0;
	$scope.fetchBooks = function(limit) {
		$http({
			url: $scope.apiURL().concat('/books'),
			method: 'GET',
			params: { page: $scope.page, limit: limit }
		}).then(function(res) {
			if(res.data.books.length == 0)
				$scope.noMoreItemsAvailable = true;
			else {
				BookService.all = BookService.all.concat(res.data.books);
				$scope.page += 1;
				$scope.$broadcast('scroll.infiniteScrollComplete');
			}
			
		}, function() {
			alert("No fué posible descargar más libros");
		});
	}
		
  $ionicPlatform.ready(function() {
    
    $ionicModal.fromTemplateUrl('templates/post-new.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.newPostModal = modal;
    });
		
    $ionicModal.fromTemplateUrl('templates/post-isbn-manual.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.manualISBNModal = modal;
    });
		
  });  
	
	$scope.searchItems = function() {
		$http({
			url: $scope.apiURL().concat('/search?term='+$scope.search.term),
			method: 'GET'
		}).then(function(res) {
			BookService.all = res.data.books;
		}, function() {
			//alert("Ocurrió un error al realizar la búsqueda");
		});
	}
	
	$scope.bookISBNLookup = function(isbn) {
		$http.get('https://www.googleapis.com/books/v1/volumes?q=isbn:'+isbn).then(function(res) {
			if(res.data.totalItems == 0) {
				alert("No encontramos el ejemplar con el ISBN provisto. Intenta con otro título.");
			} else {
				$scope.currentBookData = res.data.items[0].volumeInfo;
				$scope.newBookData.book.isbn = isbn;
				$scope.dismissManualISBNModal();
				$scope.displayNewPostForm();
			}
		}, function(error) {
			alert("Libro no encontrado");
		});
	}
	
	$scope.clickedOnManualLookup = function() {
		$scope.bookISBNLookup($scope.newBookData.book.isbn);
	}
	
	$scope.scanBarcode = function() {
		// Change this boolean direction
		if($scope.userHasSession()) {
			var isScannable = confirm("¿El ejemplar que vas a publicar tiene un código de barras?");
			
			if(isScannable) {
				alert("Toma una foto al código de barras del libro que quieres publicar");
			
		    $cordovaBarcodeScanner.scan().then(function(imageData) {
					if(!imageData.cancelled)
						$scope.bookISBNLookup(imageData.text);
		    }, function(error) {
					alert("No fué posible leer el ISBN a partir de la fotografía. Intenta de nuevo por favor");
		    });
			} else {
				$scope.displayManualISBNInputForm();
			}
		} else {
			alert("Necesitas estar logeado para publicar un ejemplar");
		}
	}
  
  $scope.showBookDetails = function(book) {
		BookService.selected = book;
		$state.go('tab.book-detail', { bookID: book.id });
  }
	
	$scope.displayManualISBNInputForm = function() {
    $scope.manualISBNModal.show();
	}
	
	$scope.displayNewPostForm = function() {
		var posOptions = {timeout: 10000, enableHighAccuracy: false};
	  $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
			$scope.location = { lat: position.coords.latitude, lng: position.coords.longitude };
	  }, function(err) {
	      // error
		});
    $scope.newPostModal.show();
	}
  
  $scope.addMedia = function() {
    $scope.hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: 'Take photo' },
        { text: 'Photo from library' }
      ],
      titleText: 'Add images',
      cancelText: 'Cancel',
      buttonClicked: function(index) {
        $scope.addImage(index);
      }
    });
  }
  
  $scope.addImage = function(type) {
    $scope.hideSheet();
    ImageFactory.handleMediaDialog(type).then(function(imageFile) {
      $scope.post.imageURL = imageFile;
    });
  }
  
  $scope.discardPost = function(callback) {
    $scope.newPostModal.hide();
    $scope.newBookData = BookService.new();
		
		if(typeof callback != "undefined")
			callback();
  }
	
	$scope.dismissManualISBNModal = function() {
    $scope.manualISBNModal.hide();
	}
  
  $scope.imageForNewPostSelected = function() {
    return $scope.post.imageURL !== null;
  }
	
	$scope.availabilityFor = function(book) {
		if(book.prints == 1) {
			return "1 ejemplar disponible";
		} else {
			return book.prints.toString().concat(" ejemplares disponibles");
		}
	}
	
	$scope.authors = function() {
		if($scope.currentBookData == null)
			return "";
		else {
			var authors = "";
			for(var idx in $scope.currentBookData.authors) {
				authors = authors.concat($scope.currentBookData.authors[idx].concat(", "));
			}
			return authors.replace(/,\s*$/, "");
		}
	}
  
  $scope.submitPost = function() {		
		$scope.setSystemToBusy(true);
		$scope.newBookData.book["title"] = $scope.currentBookData.title;
		$scope.newBookData.book["publisher"] = $scope.currentBookData.publisher;
		$scope.newBookData.book["published_date"] = $scope.currentBookData.publishedDate;
		if(typeof $scope.currentBookData.imageLinks != "undefined")
			$scope.newBookData.book["pic_url"] = $scope.currentBookData.imageLinks.thumbnail;
			
		$scope.newBookData.book["authors"] = $scope.authors();
		
		if($scope.location != null)
			$scope.newBookData.print.location = "POINT("+ $scope.location.lat + " " + $scope.location.lng + ")";
		
		$http({
			url: $scope.apiURL().concat('/books'),
			method: 'POST',
			headers: {
				"Authorization": $localstorage.getObject("user").token
			},
			data: { book: $scope.newBookData.book, print: $scope.newBookData.print }
		}).then(function(res) {
			alert("Tu ejemplar ha sido publicado para su venta");
			$scope.setSystemToBusy(false);
			BookService.all = [];
			$scope.page=0;
			$scope.discardPost(function() {
				$scope.fetchBooks(5);
			});
			
		}, function() {
			alert("No fué posible publicar tu ejemplar. Intenta nuevamente por favor.");
			$scope.setSystemToBusy(false);
		});
  }
});

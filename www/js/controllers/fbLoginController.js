controllers.controller('FBLoginController', function($scope, ngFB, $state, User, $localstorage, $http, LivbroApp) {
	
	if($scope.userHasSession()) {
    $state.go('tab.home', { });
  }
	
  $scope.data = {};
	
	$scope.login = function() {
		$scope.setSystemToBusy(true);
		var sessionParams = {
			session: { login: $scope.data.login, password: $scope.data.password }
		}
		$http.post($scope.apiURL().concat('/login.json'), sessionParams).then(function(res) {
			$scope.setSystemToBusy(false);
			$localstorage.setObject('user', { token: res.data.token, identifier: res.data.identifier }, function() {
				$state.go('tab.home', { });
			});
		}, function(err) {
			$scope.setSystemToBusy(false);
		});
	}
  
  $scope.fbLogin = function() {
    ngFB.login({scope: 'email, public_profile, publish_actions'}).then(
      function (response) {     
        if (response.status === 'connected') {
            $scope.completeLogin(response);
        } else {
            alert('Facebook login failed');
        }
    });
  }
  
	$scope.dummy = function() {
		var registrationParams = {
			authorization: { 
				email: "castillo@gm.com", 
				first_name: "Paco", 
				last_name: "Castillo",
				user_avatar: "http://graph.facebook.com/10153726790278980/picture",
				user_id: "10153726790278980",
				user_token: "EAAKGaPbRaV8BAEh67K8A6M8v8KHWn97cUd2f6iJDhJ15BnPeeg2lM3e2KDI9n0o39dEm7Ts7bfyXgJO64HWZAaeKogZCHC82e6F6Oem8pLZCJkdHPPeDuhjNLqUVih8nSwBqoliKkcZAm1KXqXoBJoPLR8pk6QoZD"
			}
		}
		$http.post($scope.apiURL().concat('/authorizations/fb.json'), registrationParams).then(function(res) {
			$localstorage.setObject('user', { token: res.data.token, identifier: res.data.identifier }, function() {
				$state.go('tab.home', { });
			});
			$scope.setSystemToBusy(false);
		}, function(err) {
			$scope.setSystemToBusy(false);
		});
	}
  
  $scope.completeLogin = function(response) {
    ngFB.api({
      path: '/me',
      params: {fields: 'id,name,email'}
    }).then(
      function (fbUser) {
				$scope.setSystemToBusy(true);
				var registrationParams = {
					authorization: { 
						email: fbUser.email, 
						first_name: fbUser.name.split(" ")[0], 
						last_name: fbUser.name.split(" ")[1],
						user_avatar: "http://graph.facebook.com/".concat(fbUser.id).concat("/picture"),
						user_id: fbUser.id,
						user_token: response.authResponse.accessToken
					}
				}
				$http.post($scope.apiURL().concat('/authorizations/fb.json'), registrationParams).then(function(res) {
					$localstorage.setObject('user', { token: res.data.token, identifier: res.data.identifier }, function() {
						$state.go('tab.home', { });
					});
					$scope.setSystemToBusy(false);
				}, function(err) {
					$scope.setSystemToBusy(false);
				});
      },
      function (error) {
          alert('Facebook error: ' + error.error_description);
      }
    );    
  } 
});
controllers.controller('LoginController', function($scope, ngFB, $state, User, $localstorage, $http, LivbroApp) {
	
	if($scope.userHasSession()) {
    $state.go('tab.home', { });
  }
	
  $scope.data = {};
	
	$scope.login = function() {
		$scope.setSystemToBusy(true);
		var sessionParams = {
			session: { login: $scope.data.login, password: $scope.data.password }
		}
		$http.post($scope.apiURL().concat('/login.json'), sessionParams).then(function(res) {
			$scope.setSystemToBusy(false);
			$localstorage.setObject('user', { token: res.data.token, identifier: res.data.identifier }, function() {
				$state.go('tab.home', { });
			});
		}, function(err) {
			$scope.setSystemToBusy(false);
		});
	}
  
  $scope.fbLogin = function() {
    ngFB.login({scope: 'email, public_profile, publish_actions'}).then(
      function (response) {     
        if (response.status === 'connected') {
            $scope.completeLogin(response);
        } else {
            alert('Facebook login failed');
        }
    });
  }
  
  
  $scope.completeLogin = function(response) {
    ngFB.api({
      path: '/me',
      params: {fields: 'id,name,email'}
    }).then(
      function (fbUser) {
        var user = new User({
          name: fbUser.name,
          facebook_id: fbUser.id,
          facebook_token: response.authResponse.accessToken,
          email: fbUser.email
        });
        
        console.log("LOKLI ".concat(response.authResponse.accessToken).concat(" ").concat(fbUser.email));
        user.$save(function(response, putResponseHeaders) {
          // Move the block below to the user factory
          $localstorage.setObject('user', { 
            session: response['session_token'],
            name: fbUser.name,
            email: fbUser.email,
            id: fbUser.id
          });
        });
      },
      function (error) {
          alert('Facebook error: ' + error.error_description);
      }
    );
    
    $state.go('tab.home', { });
  } 
});
controllers.controller('ProfileController', function($scope, $ionicModal, $ionicPlatform, $http, $localstorage, $state, $ionicActionSheet, ImageFactory, $cordovaFileTransfer) {
	$scope.image = {
		url: null
	};
	
	$scope.user = {
		first_name: "",
		last_name: ""
	};	
	
	$scope.fetchProfileDetails = function() {
		
		if($scope.userHasSession()) {
			$http.get($scope.apiURL().concat('/users/'.concat($localstorage.getObject("user").identifier)), {
				headers: {
					"Authorization": $localstorage.getObject("user").token
				}
			}).then(function(res) {
				$scope.user = res.data.user;
			});
		}
	}
	
  $ionicPlatform.ready(function() {
    
    $ionicModal.fromTemplateUrl('templates/profile-settings-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.profileSettingsModal = modal;
    });
		
    $ionicModal.fromTemplateUrl('templates/image-change-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.imageChangeModal = modal;
    });
    
		$scope.fetchProfileDetails();
  });
	
	$scope.$on("$ionicView.enter", function(event, data){
		if($scope.user == null)
			$scope.fetchProfileDetails();
	});
	
	$scope.goToLogin = function() {
		$state.go('login', { });
	}
	
	$scope.displayImageSelectModal = function() {
    $scope.imageChangeModal.show();
	}
	
	$scope.dismissImageSelectModal = function(callback) {
    $scope.imageChangeModal.hide();
		if(typeof callback != "undefined")
			callback();
	}
	
	$scope.displayProfileSettings = function() {
    $scope.profileSettingsModal.show();	
	}
	
	$scope.dismissProfileSettings = function() {
    $scope.profileSettingsModal.hide();	
	}
	
	$scope.currentUserName = function() {
		if($scope.user == null)
			return "";
		return $scope.user.first_name.concat(" ").concat($scope.user.last_name);
	}
	
	$scope.closeSession = function() {
		$localstorage.setObject("user", null);
		$scope.user = null;
		$state.go('login', { });
	}
	
  $scope.changePicture = function() {
    $scope.hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: 'Tomar una foto' },
        { text: 'Seleccionar una de la galeria' }
      ],
      titleText: 'Cambiar la imagen de perfil',
      cancelText: 'Cancelar',
      buttonClicked: function(index) {
				$scope.dismissProfileSettings();
				$scope.dismissImageSelectModal();
        $scope.addImage(index);
      }
    });
  }
  
  $scope.addImage = function(type) {
    $scope.hideSheet();
    ImageFactory.handleMediaDialog(type).then(function(imageFile) {
      $scope.image.url = imageFile;
			$scope.displayImageSelectModal();
    });
  }
	
	$scope.uploadNewAvatar = function() {
		$scope.setSystemToBusy(true);
		$cordovaFileTransfer.upload($scope.apiURL().concat('/users/change-picture'), $scope.image.url, {
			params: {
				token: $localstorage.getObject("user").token
			}
		}).then(function(result) {
				$scope.setSystemToBusy(false);
				$scope.dismissImageSelectModal(function() {
					$scope.fetchProfileDetails();
				});
		}, function(err) {
				$scope.setSystemToBusy(false);
				alert("No fué posible cambiar tu foto. Intenta de nuevo por favor");
		}, null);
	}
	
	$scope.updateProfile = function() {
		var userParams = {
			user: { 
				email: $scope.user.email, 
				password: $scope.user.password, 
				first_name: $scope.user.first_name, 
				last_name: $scope.user.last_name,
				bio: $scope.user.bio
			}
		};
		
		$scope.setSystemToBusy(true);
		
		$http({ url: $scope.apiURL().concat('/users'), method: 'POST', data: userParams, headers: {
				"Authorization": $localstorage.getObject("user").token
			} 
		}).then(function(res) {
			$scope.setSystemToBusy(false);
			$scope.profileSettingsModal.hide();
		}, function(err) {
			$scope.setSystemToBusy(false);
		});
	}
	
});
controllers.controller('BookDetailsController', function($scope, $stateParams, $ionicPlatform, $ionicModal, BookService, $http, $localstorage, $state) {
	
	$scope.page = 0
	$scope.currentReserve = null;
	
	$scope.selectedBook = function() {
		return BookService.selected;
	}
	
	$scope.fetchPrints = function(limit) {
		var bookId = null;
		
		if($scope.book != null)
			bookId = $scope.book.id;
		else
			bookId = $stateParams.bookID;
		
		$http({
			url: $scope.apiURL().concat('/books/'.concat(bookId).concat('/prints')),
			method: 'GET',
			params: { page: $scope.page, limit: limit }
		}).then(function(res) {
			$scope.page =+ 1;
			BookService.selected = res.data.book;
			BookService.printsForSelected = res.data.prints;
		}, function() {
			alert("No fué posible descargar más libros");
		});
	}
	
	$scope.prints = function() {
		return BookService.printsForSelected;
	}
	
	$scope.humanizedStateFor = function(print) {
		if(print.conditions == "like-new") {
			return "Libro como nuevo";
		} else if(print.conditions == "good") {
			return "Libro en estado aceptable";
		} else if(print.conditions == "regular") {
			return "Libro en estado regular";
		} else {
			return "En mal estado";
		}
	}
	
	$scope.reserveUser = function() {
		if($scope.currentReserve != null && $scope.currentReserve.user != null)
			return $scope.currentReserve.user.first_name+" "+$scope.currentReserve.user.last_name;
		else
			return "";
	}
 	
  $ionicPlatform.ready(function() {
    
    $ionicModal.fromTemplateUrl('templates/reserve-book.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.bookReserveModal = modal;
    });
    
  }); 
	
	$scope.dismissReserveModal = function() {
    $scope.bookReserveModal.hide();	
	}
	
	$scope.showReserveModal = function() {
		if($scope.userHasSession()) {
			$scope.bookReserveModal.show();	
		} else {
			if(confirm("Necesitas estar registrado para reservar este título. ¿Deseas registrarte ahora?"))
				$state.go('login', { });
		}	
	}
	
	$scope.sendReserveRequest = function() {
		if(typeof $scope.currentReserve.message == "undefined" || $scope.currentReserve.message == null) {
			alert("Necesitas escribir un mensaje para el vendedor para iniciar el proceso de reserva.");			
			return false;
		}
		$scope.setSystemToBusy(true);
		$http({
			url: $scope.apiURL().concat('/reserves'),
			method: 'POST',
			headers: {
				"Authorization": $localstorage.getObject("user").token
			},
			data: { reserve: { print_id: $scope.currentReserve.id, message: $scope.currentReserve.message } }
		}).then(function(res) {
			$scope.dismissReserveModal();
			alert("Tu mensaje para reservar este título ha sido enviado. Dale seguimiento a tu reserva para concretar la compra.");		
			$scope.setSystemToBusy(false);
			$state.go('tab.reserves', { });	
		}, function(res) {
			if(typeof res.data.errors.buyer != "undefined")
				alert("No fué posible reservar este título: " + res.data.errors.buyer[0]);
			else if(typeof res.data.errors.print != "undefined")
				alert("No fué posible reservar este título: " + res.data.errors.print[0]);
			else
				alert("No fué posible reservar este título. Por favor intenta de nuevo.");
			$scope.setSystemToBusy(false);
		});
	}
	
	$scope.closeDeal = function(printInstance) {
		$scope.currentReserve = printInstance;
		$scope.showReserveModal();		
	}
	
	$scope.fetchPrints(10);
});

controllers.controller('ReserveDetailsController', function($scope, $stateParams, $ionicPlatform, $ionicModal, $http, $localstorage) {
  $scope.reserve = null;
	
	$scope.fetchReserveDetails = function() {
		$http({
			url: $scope.apiURL().concat('/reserves/'+$stateParams.reserveID),
			method: 'GET',
			headers: {
				"Authorization": $localstorage.getObject("user").token
			}
		}).then(function(res) {
			$scope.reserve = res.data;
		}, function() {
			alert("No fué posible mostrar los detalles de esta reserva");
		});
	}
	
	$scope.submitMessage = function() {
		if(typeof $scope.reserve.message == "undefined" || $scope.reserve.message == null) {
			alert("Tu mensaje no puede ir en blanco");
			return false;
		}
		
		$http({
			url: $scope.apiURL().concat('/messages'),
			method: 'POST',
			headers: {
				"Authorization": $localstorage.getObject("user").token
			},
			data: { message: { reserve_id: $scope.reserve.id, content: $scope.reserve.message } }
		}).then(function(res) {
			$scope.reserve = res.data;
		}, function(res) {
				alert("No fué posible publicar tu mensaje");
		});
	}

	$scope.fetchReserveDetails();
});
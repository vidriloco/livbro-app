controllers.controller('ReservesController', function($scope, ngFB, $state, User, $localstorage, $http) {
	
	$scope.reserves = [];
	
	$scope.openReserveDetails = function(reserveID) {
    $state.go('tab.reserve-detail', { reserveID: 1 });
	}
	
  $scope.slideHasChanged = function(page) {
    
  } 
	
	$scope.fetchReserves = function() {
		if($scope.userHasSession()) {		
			$http({
				url: $scope.apiURL().concat('/reserves'),
				method: 'GET',
				headers: {
					"Authorization": $localstorage.getObject("user").token
				}
			}).then(function(res) {
				$scope.reserves = res.data;
			}, function() {
				alert("No fué posible mostrar tus reservas");
			});
		} else {
			$scope.reserves = [];
		}
	}
	
	$scope.$on("$ionicView.enter", function(event, data){
		$scope.fetchReserves();
	});
	
	
});
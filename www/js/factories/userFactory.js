factories.factory('User', function($resource, LivbroApp) {  
  // Might use a resource here that returns a JSON array
  return $resource('/api/users');
});

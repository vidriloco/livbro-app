factories.factory('Post', function($resource, LivbroApp) {
  var livbroURL = LivbroApp.host.development;
	
  // Might use a resource here that returns a JSON array
  return $resource(livbroURL.concat('/api/posts/:post'), { post: "@post" }, {
		all: function() {
			return [{
		  	title: "Headway interactive",
				price: 300,
				units: 2,
				conditions: "good",
				description: null,
				isbn: "932349-324324-3432D-342DE",
				cover_url: null,
				year: 2012,
				edition: "Segunda Edición",
				created_at: "05-01-2016 19:49:40",
				owner: {
					name: "Alejandro Numa",
					avatar_url: null
				}
		  },
		  {
		  	title: "Goals achieved",
				price: 600,
				units: 5,
				conditions: "regular",
				description: null,
				isbn: "932349-324324-3432D-342DE",
				cover_url: null,
				year: 2000,
				edition: "Primera Edición",
				created_at: "05-09-2016 19:49:40",
				owner: {
					name: "Federico Nava",
					avatar_url: null
				}
		  }];
		},
    within: {
      method: 'get',
      isArray: true,
      params: {
        distance: 200
      }
    }
  });
	
	
	
  
  // Some fake testing data
  /*var posts = [{
    id: 1,
    type: 'Post',
    title: 'Un cerrajero oportuno',
    content: 'Acabamos de abrir cerca de la anzures y por ello queremos ofrecer a todos los vecinos un trabajo con el 15%',
    cover_image: 'https://c2.staticflickr.com/4/3604/3347923854_981712f089_z.jpg?zz=1',
    interaction_count: 1,
    last_interaction_at: '2015-11-05T08:15:30-05:00',
    distance: 50,
    author: {
      id: 1,
      name: 'Katia',
      avatar_image: 'https://c1.staticflickr.com/3/2347/2301804477_44569f418a_q.jpg'
    },
    interactions: [
      {
        id: 2,
        replied_to: 4,
        author: {
          id: 2,
          name: 'Joaquin',
          avatar_image: 'https://c2.staticflickr.com/4/3859/14488884453_a2baec8c83_q.jpg'
        },
        date: '2015-11-05T08:15:30-05:00',
        content: 'No me parece buena tu propuesta'
      }
    ]
  }, {
    id: 2,
    type: 'Post',
    title: 'Quiero regresar a casa',
    content: 'Me perdí hace unas semanas y mis dueños seguramente me extrañan: ayudame a regresar a casa',
    cover_image: 'https://c2.staticflickr.com/8/7055/6795956662_7e8de8f4b9_b.jpg',
    interaction_count: 2,
    last_interaction_at: '2015-11-05T08:15:30-05:00',
    distance: 75,
    author: {
      id: 1,
      name: 'Luis',
      avatar_image: 'https://c1.staticflickr.com/3/2763/5700872954_ae8db01d45_q.jpg'
    },
    interactions: [
      {
        id: 2,
        replied_to: 4,
        author: {
          id: 2,
          name: 'Joaquin',
          avatar_image: 'https://c2.staticflickr.com/4/3859/14488884453_a2baec8c83_q.jpg'
        },
        date: '2015-11-05T08:15:30-05:00',
        content: 'No me parece buena tu propuesta, es muy revoloteada!'
      },
      {
        id: 3,
        replied_to: 1,
        author: {
          id: 2,
          name: 'Joaquin',
          avatar_image: 'https://c2.staticflickr.com/4/3859/14488884453_a2baec8c83_q.jpg'
        },
        date: '2015-11-05T08:15:30-05:00',
        content: 'Bueno, esta bien'
      }
    ]
  }];

  return {
    all: function() {
      return posts;
    },
    remove: function(post) {
      posts.splice(posts.indexOf(post), 1);
    },
    get: function(postId) {
      for (var i = 0; i < posts.length; i++) {
        if (posts[i].id === parseInt(postId)) {
          return posts[i];
        }
      }
      return null;
    }return $resource('/api/post/:id');
  };*/
});
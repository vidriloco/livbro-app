factories.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value, callback) {
      $window.localStorage[key] = JSON.stringify(value);
			if(typeof callback !== "undefined")
				callback();
    },
    getObject: function(key) {
      var value = $window.localStorage[key];
      if(value != null && typeof(value) !== undefined)
        return JSON.parse(value);
      else
        return false;
    }
  }
}]);
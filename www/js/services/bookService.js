services.service('BookService', function() {
  return {
  	new: function() {
			return {
					book: { isbn: null, title: null, publisher: null, authors: null, published_date: null },
					print: {
						price: 0,
						units: 0,
						conditions: "good",
						description: null
					}
				};
  	},
		all: [],
		selected: null,
		printsForSelected: []
  }
});
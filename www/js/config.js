// Settings for production environment
app.constant('LivbroApp', {
  host: {
    production: 'http://50.56.30.227:3001/api',
    development: 'http://192.168.0.36:3000/api'
  }
});
